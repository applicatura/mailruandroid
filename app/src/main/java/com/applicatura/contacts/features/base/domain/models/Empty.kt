package com.applicatura.contacts.features.base.domain.models

import com.applicatura.contacts.base.items.DiffItem

object Empty : DiffItem {

    override val itemId: String
        get() = "empty_list"

    override val itemHash: Int
        get() = hashCode()
}
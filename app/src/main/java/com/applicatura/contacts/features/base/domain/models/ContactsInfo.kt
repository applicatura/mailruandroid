package com.applicatura.contacts.features.base.domain.models

import android.os.Parcelable
import com.applicatura.contacts.R
import com.applicatura.contacts.base.items.DiffItem
import kotlinx.android.parcel.IgnoredOnParcel
import kotlinx.android.parcel.Parcelize
import kotlin.math.min


@Parcelize
data class ContactInfo(
    val id: Long,
    val name: String,
    val thumbnail: String?,
    val photo: String?
) : DiffItem, Parcelable {
    val backgroundPhoto = randomBackgroundColor()

    @IgnoredOnParcel
    private val _phones: MutableList<String> = mutableListOf()

    @IgnoredOnParcel
    val phones: List<String> = _phones

    fun addPhone(phone: String) {
        _phones.add(phone)
    }

    override val itemId: String
        get() = id.toString()

    override val itemHash: Int
        get() = hashCode()
}

fun ContactInfo.firstLetters(): String {
    val split = name
        .split(" ")
        .map {
            it
                .replace("(", "")
                .replace(")", "")
        }
    return split.take(min(split.size, 2)).joinToString(separator = "") { it.getFirstLetters() }
}

private fun String.getFirstLetters() = substring(0, 1)


private fun randomBackgroundColor() = listOf(
    R.attr.colorAvatar1,
    R.attr.colorAvatar2,
    R.attr.colorAvatar3,
    R.attr.colorAvatar4,
    R.attr.colorAvatar5,
).random()
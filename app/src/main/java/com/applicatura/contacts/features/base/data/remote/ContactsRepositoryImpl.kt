package com.applicatura.contacts.features.base.data.remote

import android.annotation.SuppressLint
import android.content.Context
import android.database.Cursor
import android.provider.ContactsContract
import com.applicatura.contacts.features.base.data.ContactsRepository
import com.applicatura.contacts.features.base.domain.models.ContactInfo

data class ContactsRepositoryImpl(val context: Context) : ContactsRepository {

    @SuppressLint("Recycle")
    override suspend fun getContacts(): List<ContactInfo> {
        val contacts: MutableList<ContactInfo> = mutableListOf()
        val contentResolver = context.contentResolver
        val contactsCursor = contentResolver.query(
            CONTENT_URI, null, null, null, DISPLAY_NAME
        )
        contactsCursor?.use {
            while (contactsCursor.count > 0 && contactsCursor.moveToNext()) {
                val contactId = contactsCursor.getLong(ID)
                val name = contactsCursor.getString(DISPLAY_NAME)
                val photo = contactsCursor.getString(PHOTO)
                val thumbnail = contactsCursor.getString(THUMBNAIL)
                val hasPhoneNumber = contactsCursor.getInt(HAS_PHONE_NUMBER)
                name?.let {
                    if (hasPhoneNumber > 0) {
                        val contactInfo = ContactInfo(
                            id = contactId,
                            name = name,
                            thumbnail = thumbnail,
                            photo = photo
                        )
                        val phoneCursor = contentResolver.query(
                            PHONE_URI, arrayOf(PHONE_NUMBER),
                            "$PHONE_ID = ?", arrayOf(contactId.toString()), null
                        )
                        phoneCursor?.use {
                            while (phoneCursor.count > 0 && phoneCursor.moveToNext()) {
                                val phone = phoneCursor.getString(PHONE_NUMBER)
                                contactInfo.addPhone(phone)
                            }
                            contacts.add(contactInfo)
                        }
                    }
                }
            }
        }

        return contacts.toList()
    }

    private fun Cursor.getString(columnName: String) = getString(getColumnIndex(columnName))
    private fun Cursor.getLong(columnName: String) = getLong(getColumnIndex(columnName))
    private fun Cursor.getInt(columnName: String) = getInt(getColumnIndex(columnName))

    companion object {

        private val CONTENT_URI = ContactsContract.Contacts.CONTENT_URI
        private const val ID = ContactsContract.Contacts._ID
        private const val DISPLAY_NAME = ContactsContract.Contacts.DISPLAY_NAME
        private const val PHOTO = ContactsContract.Contacts.PHOTO_URI
        private const val THUMBNAIL = ContactsContract.Contacts.PHOTO_THUMBNAIL_URI
        private const val HAS_PHONE_NUMBER = ContactsContract.Contacts.HAS_PHONE_NUMBER
        private val PHONE_URI = ContactsContract.CommonDataKinds.Phone.CONTENT_URI
        private const val PHONE_ID = ContactsContract.CommonDataKinds.Phone.CONTACT_ID
        private const val PHONE_NUMBER = ContactsContract.CommonDataKinds.Phone.NUMBER
    }
}
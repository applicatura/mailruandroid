package com.applicatura.contacts.features.contact_info.di

import com.applicatura.contacts.features.base.domain.models.ContactInfo
import com.applicatura.contacts.features.contact_info.ui.ContactInfoViewModel
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

val contactInfoModule = module {

    viewModel { (contact: ContactInfo) ->
        ContactInfoViewModel(
            contact = contact,
            router = get()
        )
    }
}
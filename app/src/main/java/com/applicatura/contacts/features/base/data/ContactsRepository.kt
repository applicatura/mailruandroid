package com.applicatura.contacts.features.base.data

import com.applicatura.contacts.features.base.domain.models.ContactInfo

interface ContactsRepository {

    suspend fun getContacts(): List<ContactInfo>

}
package com.applicatura.contacts.features.main.ui

import androidx.lifecycle.viewModelScope
import com.applicatura.contacts.R
import com.applicatura.contacts.base.navigation.MainActivityRouter
import com.applicatura.contacts.base.navigation.SystemScreens
import com.applicatura.contacts.base.platform.BaseViewModel
import com.applicatura.contacts.base.platform.ErrorEvent
import com.applicatura.contacts.base.platform.Event
import com.applicatura.contacts.base.utils.ResourcesProvider
import com.applicatura.contacts.base.utils.SingleLiveEvent
import com.applicatura.contacts.features.base.Screens
import com.applicatura.contacts.features.base.domain.ContactsInteractor
import kotlinx.coroutines.launch

class MainViewModel(
    private val router: MainActivityRouter,
    private val interactor: ContactsInteractor,
    private val resourcesProvider: ResourcesProvider
) : BaseViewModel<ViewState>() {

    val singleLiveEvent = SingleLiveEvent<SingleEvent>()

    override fun initialViewState() = ViewState(state = State.CHECKING_PERMISSIONS)

    override fun reduce(event: Event, previousState: ViewState): ViewState? {
        when (event) {
            is MainDataEvent.RequestContacts, MainUiEvent.OnContactsRefreshed -> {
                loadContacts()
                return null
            }
            is MainDataEvent.OnContactsLoadingStarted -> {
                return previousState.copy(state = State.LOADING, error = null)
            }
            is MainDataEvent.OnPermissionNotGranted -> {
                return previousState.copy(
                    state = State.PERMISSION_ERROR,
                    error = resourcesProvider.getString(R.string.permission_error)
                )
            }
            is MainDataEvent.OnEmptyContactsLoadingSucceed -> {
                return previousState.copy(
                    state = State.CONTENT_EMPTY,
                    error = resourcesProvider.getString(R.string.contacts_empty)
                )
            }
            is MainDataEvent.OnContactsLoadingSucceed -> {
                if (event.items.isEmpty()) {
                    processDataEvent(MainDataEvent.OnEmptyContactsLoadingSucceed)
                    return null
                }
                return previousState.copy(items = event.items, state = State.CONTENT, error = null)
            }
            is MainUiEvent.OnShowContactInfoClicked -> {
                router.navigateTo(Screens.ContactInfoScreen(event.contactInfo))
                return null
            }
            is MainUiEvent.OnShowPermissionError -> {
                processDataEvent(MainDataEvent.OnPermissionNotGranted)
                if (event.withPermissionDialog) {
                    singleLiveEvent.value = SingleEvent.ShowPermissionSettingsDialog
                }
                return null
            }
            is MainUiEvent.ShowPermissionSettingsScreen -> {
                router.navigateTo(SystemScreens.PermissionSettingsScreen())
                return null
            }
        }
        return null
    }

    private fun loadContacts() {
        processDataEvent(MainDataEvent.OnContactsLoadingStarted)
        viewModelScope.launch {
            interactor.getContacts().fold(
                onError = { error ->
                    processErrorEvent(MainDataEvent.OnContactsLoadingFailed(error, error.message.orEmpty()))
                },
                onSuccess = { contacts ->
                    processDataEvent(MainDataEvent.OnContactsLoadingSucceed(contacts))
                })
        }
    }

    override fun onHandleErrorEvent(event: ErrorEvent, previousState: ViewState): ViewState? = when (event) {
        is MainDataEvent.OnContactsLoadingFailed -> {
            previousState.copy(
                state = State.ERROR,
                error = if (event.message.isEmpty())
                    resourcesProvider.getString(R.string.contacts_error) else event.message
            )
        }
        else -> null
    }
}
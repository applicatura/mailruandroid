package com.applicatura.contacts.features.contact_info.ui

import com.applicatura.contacts.base.navigation.MainActivityRouter
import com.applicatura.contacts.base.navigation.SystemScreens
import com.applicatura.contacts.base.platform.BaseViewModel
import com.applicatura.contacts.base.platform.ErrorEvent
import com.applicatura.contacts.base.platform.Event
import com.applicatura.contacts.features.base.domain.models.ContactInfo

class ContactInfoViewModel(
    private val contact: ContactInfo,
    private val router: MainActivityRouter
) : BaseViewModel<ViewState>() {

    override fun initialViewState() = ViewState(contact)

    override fun reduce(event: Event, previousState: ViewState): ViewState? {
        when (event) {
            is ContactInfoUiEvent.OnBackClicked -> {
                router.exit()
                return null
            }
            is ContactInfoUiEvent.OnContactCalled -> {
                router.navigateTo(SystemScreens.DialPhoneScreen(event.phone))
                return null
            }
        }
        return null
    }

    override fun onHandleErrorEvent(event: ErrorEvent, previousState: ViewState): ViewState? {
        return null
    }
}
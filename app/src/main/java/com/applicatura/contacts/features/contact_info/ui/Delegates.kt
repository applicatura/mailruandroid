package com.applicatura.contacts.features.contact_info.ui

import com.applicatura.contacts.base.extensions.setThrottledClickListener
import com.applicatura.contacts.databinding.ItemPhoneBinding
import com.hannesdorfmann.adapterdelegates4.dsl.adapterDelegateViewBinding

fun phoneDelegate(onPhoneClicked: (phone: String) -> Unit) =
    adapterDelegateViewBinding<String, String, ItemPhoneBinding>(
        { layoutInflater, root -> ItemPhoneBinding.inflate(layoutInflater, root, false) }) {

        itemView.setThrottledClickListener { onPhoneClicked(item) }

        bind {
            binding.tvPhone.text = item
        }
    }

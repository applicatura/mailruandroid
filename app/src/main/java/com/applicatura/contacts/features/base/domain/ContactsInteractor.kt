package com.applicatura.contacts.features.base.domain

import com.applicatura.contacts.base.extensions.attempt
import com.applicatura.contacts.base.utils.Either
import com.applicatura.contacts.features.base.data.ContactsRepository
import com.applicatura.contacts.features.base.domain.models.ContactInfo

class ContactsInteractor(
    private val repository: ContactsRepository
) {

    suspend fun getContacts(): Either<Throwable, List<ContactInfo>> =
        attempt { repository.getContacts() }

}
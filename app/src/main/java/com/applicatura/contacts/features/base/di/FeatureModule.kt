package com.applicatura.contacts.features.base.di

import com.applicatura.contacts.features.base.data.ContactsRepository
import com.applicatura.contacts.features.base.data.remote.ContactsRepositoryImpl
import com.applicatura.contacts.features.base.domain.ContactsInteractor
import org.koin.dsl.module

val baseModule = module {

    single {
        ContactsInteractor(
            repository = get()
        )
    }

    single<ContactsRepository> { ContactsRepositoryImpl(context = get()) }
}
package com.applicatura.contacts.features.main.di

import com.applicatura.contacts.features.main.ui.MainViewModel
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

val mainModule = module {

    viewModel {
        MainViewModel(
            router = get(),
            interactor = get(),
            resourcesProvider = get()
        )
    }
}
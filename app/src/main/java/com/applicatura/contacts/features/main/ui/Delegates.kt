package com.applicatura.contacts.features.main.ui

import com.applicatura.contacts.base.extensions.loadImage
import com.applicatura.contacts.base.extensions.resolveThemeColorId
import com.applicatura.contacts.base.extensions.setThrottledClickListener
import com.applicatura.contacts.base.extensions.showIf
import com.applicatura.contacts.base.items.DiffItem
import com.applicatura.contacts.databinding.ItemContactBinding
import com.applicatura.contacts.features.base.domain.models.ContactInfo
import com.applicatura.contacts.features.base.domain.models.firstLetters
import com.hannesdorfmann.adapterdelegates4.dsl.adapterDelegateViewBinding

fun contactsDelegate(
    onShowContactInfoClicked: (ContactInfo) -> Unit
) =
    adapterDelegateViewBinding<ContactInfo, DiffItem, ItemContactBinding>(
        { layoutInflater, root -> ItemContactBinding.inflate(layoutInflater, root, false) }) {

        itemView.setThrottledClickListener { onShowContactInfoClicked(item) }

        bind {
            binding.tvFirstLetters.showIf { item.thumbnail.isNullOrEmpty() }
            if (item.thumbnail.isNullOrEmpty()) {
                binding.ivAvatar.setImageResource(context.resolveThemeColorId(item.backgroundPhoto))
                binding.tvFirstLetters.text = item.firstLetters()
            } else {
                binding.ivAvatar.loadImage(item.thumbnail)
            }
            binding.tvName.text = item.name
            binding.tvPhone.text = if (item.phones.isNotEmpty()) item.phones.first() else ""
        }
    }
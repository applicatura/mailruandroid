package com.applicatura.contacts.features.main.ui

import android.Manifest
import androidx.lifecycle.Observer
import by.kirich1409.viewbindingdelegate.viewBinding
import com.applicatura.contacts.R
import com.applicatura.contacts.base.extensions.*
import com.applicatura.contacts.base.items.DiffCallback
import com.applicatura.contacts.base.platform.BaseFragment
import com.applicatura.contacts.base.ui.decorators.DividerItemDecoration
import com.applicatura.contacts.databinding.FragmentMainBinding
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import com.hannesdorfmann.adapterdelegates4.AsyncListDifferDelegationAdapter
import org.koin.androidx.viewmodel.ext.android.viewModel
import permissions.dispatcher.NeedsPermission
import permissions.dispatcher.OnNeverAskAgain
import permissions.dispatcher.OnPermissionDenied
import permissions.dispatcher.RuntimePermissions

@RuntimePermissions
class MainFragment : BaseFragment<ViewState>(R.layout.fragment_main) {

    private val viewBinding: FragmentMainBinding by viewBinding(FragmentMainBinding::bind)
    override val viewModel: MainViewModel by viewModel()

    private val adapter by lazy {
        AsyncListDifferDelegationAdapter(
            DiffCallback,
            contactsDelegate(
                onShowContactInfoClicked = {
                    viewModel.processUiEvent(
                        MainUiEvent.OnShowContactInfoClicked(it)
                    )
                },
            )
        )
    }

    override fun setupUI() {
        viewBinding.toolbar.title = getString(R.string.title_main)
        viewBinding.rvContent.setAdapterAndCleanupOnDetachFromWindow(adapter)
        viewBinding.rvContent.addItemDecoration(
            DividerItemDecoration(
                requireContext().resolveThemeColor(R.attr.colorSurfaceNative),
                requireContext().dpToPx(1f),
                marginStart = requireContext().dpToPx(16f).toFloat(),
                withLastItem = true
            )
        )
        viewBinding.srl.setOnRefreshListener { getContactsWithPermissionCheck() }
        viewBinding.btnRefresh.setThrottledClickListener { getContactsWithPermissionCheck() }
    }

    override fun setupObserve() {
        super.setupObserve()
        viewModel.singleLiveEvent.observe(viewLifecycleOwner, Observer(::handleSingleLiveEvent))
    }

    private fun handleSingleLiveEvent(singleEvent: SingleEvent) {
        when (singleEvent) {
            is SingleEvent.ShowPermissionSettingsDialog -> showAccessDeniedDialog()
        }
    }

    private fun showAccessDeniedDialog() {
        MaterialAlertDialogBuilder(requireContext())
            .setTitle(R.string.access_contacts_denied_title)
            .setMessage(R.string.access_contacts_denied_description)
            .setNegativeButton(R.string.cancel) { _, _ -> }
            .setPositiveButton(R.string.settings) { _, _ ->
                viewModel.processUiEvent(MainUiEvent.ShowPermissionSettingsScreen)
            }
            .show()
    }

    override fun render(viewState: ViewState) {
        viewBinding.srl.isRefreshing = viewState.isLoadingVisible
        viewBinding.srl.showIf { viewState.isErrorVisible.not() }
        viewBinding.layoutError.showIf { viewState.isErrorVisible }

        adapter.items = viewState.items
        viewState.error?.let { viewBinding.tvError.text = it }
        if (viewState.isCheckingPermissionVisible) {
            getContactsWithPermissionCheck()
        }
    }

    @NeedsPermission(Manifest.permission.READ_CONTACTS)
    fun getContacts() {
        viewModel.processUiEvent(MainUiEvent.OnContactsRefreshed)
    }

    @OnPermissionDenied(Manifest.permission.READ_CONTACTS)
    fun onContactsPermissionDenied() {
        viewModel.processUiEvent(MainUiEvent.OnShowPermissionError(withPermissionDialog = false))
    }

    @OnNeverAskAgain(Manifest.permission.READ_CONTACTS)
    fun onContactsPermissionNeverAskAgainSelected() {
        viewModel.processUiEvent(MainUiEvent.OnShowPermissionError(withPermissionDialog = true))
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        onRequestPermissionsResult(requestCode, grantResults)
    }
}

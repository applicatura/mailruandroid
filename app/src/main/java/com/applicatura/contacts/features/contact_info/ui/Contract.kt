package com.applicatura.contacts.features.contact_info.ui

import com.applicatura.contacts.base.platform.UiEvent
import com.applicatura.contacts.features.base.domain.models.ContactInfo

data class ViewState(
    val contact: ContactInfo
) {
    val phones = contact.phones
    val name = contact.name
    val photo = contact.photo
    val backgroundPhoto = contact.backgroundPhoto
}

sealed class ContactInfoUiEvent : UiEvent {
    object OnBackClicked : ContactInfoUiEvent()

    data class OnContactCalled(val phone: String) : ContactInfoUiEvent()
}
package com.applicatura.contacts.features.main.ui

import com.applicatura.contacts.base.platform.DataEvent
import com.applicatura.contacts.base.platform.ErrorEvent
import com.applicatura.contacts.base.platform.Event
import com.applicatura.contacts.base.platform.UiEvent
import com.applicatura.contacts.features.base.domain.models.ContactInfo

enum class State {
    CHECKING_PERMISSIONS, PERMISSION_ERROR, CONTENT, CONTENT_EMPTY, LOADING, ERROR
}

data class ViewState(
    val state: State,
    val items: List<ContactInfo> = emptyList(),
    val error: String? = null
) {
    val isLoadingVisible: Boolean = state == State.LOADING
    val isErrorVisible: Boolean =
        state == State.ERROR || state == State.PERMISSION_ERROR || state == State.CONTENT_EMPTY
    val isCheckingPermissionVisible: Boolean = state == State.CHECKING_PERMISSIONS
}

sealed class MainUiEvent : UiEvent {
    object OnContactsRefreshed : MainUiEvent()

    data class OnShowContactInfoClicked(val contactInfo: ContactInfo) : MainUiEvent()

    data class OnShowPermissionError(val withPermissionDialog: Boolean) : MainUiEvent()

    object ShowPermissionSettingsScreen : MainUiEvent()
}

sealed class MainDataEvent : DataEvent {
    object RequestContacts : MainDataEvent()

    object OnContactsLoadingStarted : MainDataEvent()
    data class OnContactsLoadingSucceed(val items: List<ContactInfo>) : MainDataEvent()
    object OnEmptyContactsLoadingSucceed : MainDataEvent()
    data class OnContactsLoadingFailed(override val error: Throwable, val message: String) : MainDataEvent(), ErrorEvent

    object OnPermissionNotGranted : MainDataEvent()
}

sealed class SingleEvent : Event {

    object ShowPermissionSettingsDialog : SingleEvent()
}

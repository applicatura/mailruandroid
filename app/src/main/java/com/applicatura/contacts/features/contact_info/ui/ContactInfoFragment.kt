package com.applicatura.contacts.features.contact_info.ui

import androidx.core.os.bundleOf
import by.kirich1409.viewbindingdelegate.viewBinding
import com.applicatura.contacts.R
import com.applicatura.contacts.base.extensions.*
import com.applicatura.contacts.base.platform.BaseFragment
import com.applicatura.contacts.base.ui.decorators.DividerItemDecoration
import com.applicatura.contacts.databinding.FragmentContactInfoBinding
import com.applicatura.contacts.features.base.domain.models.ContactInfo
import com.hannesdorfmann.adapterdelegates4.ListDelegationAdapter
import org.koin.androidx.viewmodel.ext.android.viewModel
import org.koin.core.parameter.parametersOf

class ContactInfoFragment : BaseFragment<ViewState>(R.layout.fragment_contact_info) {

    private val viewBinding: FragmentContactInfoBinding by viewBinding(FragmentContactInfoBinding::bind)
    override val viewModel: ContactInfoViewModel by viewModel {
        parametersOf(arguments?.getParcelable(ARG_CONTACT))
    }

    private val adapter by lazy {
        ListDelegationAdapter(
            phoneDelegate { viewModel.processUiEvent(ContactInfoUiEvent.OnContactCalled(it)) },
        )
    }

    override fun setupUI() {
        viewBinding.toolbar.setNavigationIcon(R.drawable.ic_back)
        viewBinding.toolbar.setNavigationOnClickListener { viewModel.processUiEvent(ContactInfoUiEvent.OnBackClicked) }
        viewBinding.rvContent.setAdapterAndCleanupOnDetachFromWindow(adapter)
        viewBinding.rvContent.addItemDecoration(
            DividerItemDecoration(
                requireContext().resolveThemeColor(R.attr.colorSurfaceNative),
                requireContext().dpToPx(1f),
                withLastItem = true
            )
        )
    }

    override fun render(viewState: ViewState) {
        adapter.setData(viewState.phones)
        viewBinding.collapsingToolbarLayout.title = viewState.name
        if (viewState.photo.isNullOrEmpty()) {
            viewBinding.ivAvatar.setImageResource(
                requireContext().resolveThemeColorId(viewState.backgroundPhoto)
            )
        } else {
            viewBinding.ivAvatar.loadImage(viewState.photo)
        }
    }

    companion object {
        private const val ARG_CONTACT = "ARG_CONTACT"
        fun newInstance(contact: ContactInfo) =
            ContactInfoFragment().apply {
                arguments = bundleOf(
                    ARG_CONTACT to contact
                )
            }
    }
}

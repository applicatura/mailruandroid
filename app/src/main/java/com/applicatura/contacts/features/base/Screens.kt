package com.applicatura.contacts.features.base

import com.applicatura.contacts.features.base.domain.models.ContactInfo
import com.applicatura.contacts.features.contact_info.ui.ContactInfoFragment
import com.applicatura.contacts.features.main.ui.MainFragment
import ru.terrakok.cicerone.android.support.SupportAppScreen

sealed class Screens : SupportAppScreen() {

    object MainScreen : Screens() {
        override fun getFragment() = MainFragment()
    }

    data class ContactInfoScreen(private val contact: ContactInfo) : Screens() {
        override fun getFragment() = ContactInfoFragment.newInstance(contact)
    }
}

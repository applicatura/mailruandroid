package com.applicatura.contacts.base.extensions

import android.content.Context
import android.graphics.drawable.Drawable
import android.util.TypedValue
import android.view.inputmethod.InputMethodManager
import androidx.annotation.AttrRes
import androidx.annotation.ColorInt
import androidx.annotation.ColorRes
import androidx.annotation.DrawableRes
import androidx.core.content.ContextCompat

val Context.inputMethodManager: InputMethodManager
    get() = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager

@ColorInt
fun Context.color(@ColorRes colorRes: Int): Int {
    return ContextCompat.getColor(this, colorRes)
}

fun Context.drawable(@DrawableRes drawableRes: Int): Drawable {
    return ContextCompat.getDrawable(this, drawableRes)!!
}

fun Context.dpToPx(dp: Float): Int = (dp * resources.displayMetrics.density).toInt()

@ColorInt
fun Context.resolveThemeColor(@AttrRes attrResColor: Int): Int {
    val typedValue = TypedValue()
    theme.resolveAttribute(attrResColor, typedValue, true)
    return typedValue.data
}
@ColorRes
fun Context.resolveThemeColorId(@AttrRes attrResColor: Int): Int {
    val typedValue = TypedValue()
    theme.resolveAttribute(attrResColor, typedValue, false)
    return typedValue.data
}
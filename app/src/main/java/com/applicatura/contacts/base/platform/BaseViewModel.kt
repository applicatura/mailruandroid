package com.applicatura.contacts.base.platform

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import timber.log.Timber

interface Event
interface UiEvent : Event
interface DataEvent : Event
interface ErrorEvent : Event {
    val error: Throwable
}

abstract class BaseViewModel<VIEW_STATE> : ViewModel() {

    val viewState: MutableLiveData<VIEW_STATE> by lazy { MutableLiveData(initialViewState()) }

    abstract fun initialViewState(): VIEW_STATE

    abstract fun reduce(event: Event, previousState: VIEW_STATE): VIEW_STATE?

    protected abstract fun onHandleErrorEvent(event: ErrorEvent, previousState: VIEW_STATE): VIEW_STATE?

    protected open fun onAfterStateChanged(oldViewState: VIEW_STATE, newViewState: VIEW_STATE, event: Event) {

    }

    fun processUiEvent(event: UiEvent) {
        updateState(event)
    }

    protected fun processDataEvent(event: DataEvent) {
        updateState(event)
    }

    private fun updateState(event: Event) {
        val newViewState = reduce(event, viewState.value ?: initialViewState())
        compareNewStateWithCurrentAndUpdate(newViewState, event)
    }

    protected open fun onDefaultErrorHandlingHappened(previousState: VIEW_STATE): VIEW_STATE? {
        return previousState
    }

    protected fun processErrorEvent(errorEvent: ErrorEvent) {
        val newViewState = if (processError(errorEvent.error)) {
            onDefaultErrorHandlingHappened(viewState.value!!)
        } else {
            onHandleErrorEvent(event = errorEvent, previousState = viewState.value!!)
        }
        compareNewStateWithCurrentAndUpdate(newViewState, errorEvent)
    }

    private fun processError(exception: Throwable): Boolean {
        Timber.e(exception)
        //val apiException = (exception as? ApiException) ?: exception.cause as? ApiException
        return false
    }

    private fun compareNewStateWithCurrentAndUpdate(newViewState: VIEW_STATE?, event: Event) {
        if (newViewState != null && newViewState != viewState.value) {
            val oldViewState = viewState.value
            viewState.value = newViewState
            onAfterStateChanged(oldViewState!!, newViewState, event)
        }
    }

}

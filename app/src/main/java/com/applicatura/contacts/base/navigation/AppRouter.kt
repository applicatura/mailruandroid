package com.applicatura.contacts.base.navigation

import ru.terrakok.cicerone.Router
import ru.terrakok.cicerone.Screen
import ru.terrakok.cicerone.android.support.SupportAppScreen
import ru.terrakok.cicerone.commands.Command

data class Add(val screen: Screen) : Command

data class StartWithResult(val screen: SupportAppScreen, val requestCode: Int) : Command

open class AppRouter(qualifierName: String) : Router() {

    val name = qualifierName

    fun addTo(screen: Screen) {
        executeCommands(Add(screen))
    }

    fun startWithResult(screen: SupportAppScreen, requestCode: Int) {
        executeCommands(StartWithResult(screen, requestCode))
    }
}
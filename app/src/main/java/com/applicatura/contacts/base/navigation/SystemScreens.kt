package com.applicatura.contacts.base.navigation

import android.content.Context
import android.content.Intent
import android.net.Uri
import android.provider.Settings
import ru.terrakok.cicerone.android.support.SupportAppScreen

sealed class SystemScreens : SupportAppScreen() {

    data class DialPhoneScreen(private val phone: String) : SystemScreens() {
        override fun getActivityIntent(context: Context) =
            Intent(Intent.ACTION_DIAL, Uri.parse("tel:$phone"))
    }

    class PermissionSettingsScreen : SystemScreens() {
        override fun getActivityIntent(context: Context) =
            Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS)
                .addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                .setData(
                    Uri.fromParts(
                        "package",
                        context.packageName,
                        null
                    )
                )
    }
}
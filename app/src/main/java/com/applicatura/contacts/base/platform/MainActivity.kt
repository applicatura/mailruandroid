package com.applicatura.contacts.base.platform

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.applicatura.contacts.R
import com.applicatura.contacts.base.navigation.AppNavigator
import com.applicatura.contacts.base.navigation.MainActivityRouter
import com.applicatura.contacts.di.DIQualifiers.navigationHolderQualifier
import com.applicatura.contacts.di.initializer.MAIN_ACTIVITY_QUALIFIER
import com.applicatura.contacts.features.base.Screens
import org.koin.android.ext.android.inject
import ru.terrakok.cicerone.NavigatorHolder

class MainActivity : AppCompatActivity() {

    private val navigator by lazy {
        AppNavigator(this, supportFragmentManager, R.id.fragmentContainer)
    }

    private val navigatorHolder: NavigatorHolder by inject(
        navigationHolderQualifier(
            MAIN_ACTIVITY_QUALIFIER
        )
    )
    private val router: MainActivityRouter by inject()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        router.newRootScreen(Screens.MainScreen)
    }

    override fun onResumeFragments() {
        super.onResumeFragments()
        navigatorHolder.setNavigator(navigator)
    }

    override fun onPause() {
        super.onPause()
        navigatorHolder.removeNavigator()
    }
}
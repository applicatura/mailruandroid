package com.applicatura.contacts.base.items

interface DiffItem {
    val itemId: String
    val itemHash: Int
}
package com.applicatura.contacts.base.navigation

import com.applicatura.contacts.di.DIQualifiers
import com.applicatura.contacts.di.initializer.MAIN_ACTIVITY_QUALIFIER

class MainActivityRouter : AppRouter(DIQualifiers.routerQualifier(MAIN_ACTIVITY_QUALIFIER).value)
package com.applicatura.contacts.base.extensions

import android.app.Activity

fun Activity.hideKeyboard() {
    inputMethodManager.hideSoftInputFromWindow(window.decorView.windowToken, 0)
}
package com.applicatura.contacts.base.platform

import android.os.Bundle
import android.view.View
import androidx.annotation.LayoutRes
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import com.applicatura.contacts.base.extensions.hideKeyboard

abstract class BaseFragment<VIEW_STATE>(
    @LayoutRes contentLayoutId: Int
) : Fragment(contentLayoutId) {

    open val viewModel: BaseViewModel<VIEW_STATE>? = null

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        setupUI()
        setupObserve()
    }

    open fun setupUI() {

    }

    open fun setupObserve() {
        viewModel?.viewState?.observe(viewLifecycleOwner, Observer(::render))
    }

    open fun render(viewState: VIEW_STATE) {

    }

    open fun processError() {
    }

    override fun onStop() {
        super.onStop()
        hideKeyboard()
    }

}

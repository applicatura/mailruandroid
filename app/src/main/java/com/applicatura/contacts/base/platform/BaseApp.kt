package com.applicatura.contacts.base.platform

import android.app.Application
import com.applicatura.contacts.di.appModule
import com.applicatura.contacts.features.base.di.baseModule
import com.applicatura.contacts.features.contact_info.di.contactInfoModule
import com.applicatura.contacts.features.main.di.mainModule
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidLogger
import org.koin.core.context.startKoin
import timber.log.Timber

class BaseApp : Application() {

    override fun onCreate() {
        super.onCreate()
        initLogger()
        initKoin()
    }
    private fun initLogger() {
        Timber.plant(Timber.DebugTree())
    }

    private fun initKoin() {
        startKoin {
            androidContext(this@BaseApp)
            androidLogger()
            modules(
                listOf(
                    appModule,
                    baseModule,
                    mainModule,
                    contactInfoModule
                )
            )
        }
    }
}

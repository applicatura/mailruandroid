package com.applicatura.contacts.base.extensions

import android.graphics.Paint
import android.graphics.drawable.Drawable
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import androidx.annotation.ColorInt
import androidx.annotation.ColorRes
import androidx.annotation.DrawableRes
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.RequestBuilder

const val DEFAULT_THROTTLE_DELAY = 200L

fun View.show() {
    visibility = View.VISIBLE
}

fun View.hide() {
    visibility = View.GONE
}

inline fun View.showIf(condition: View.() -> Boolean) {
    if (condition()) {
        show()
    } else {
        hide()
    }
}

@ColorInt
fun View.color(@ColorRes colorRes: Int): Int {
    return ContextCompat.getColor(context, colorRes)
}

fun View.drawable(@DrawableRes drawableRes: Int): Drawable {
    return ContextCompat.getDrawable(context, drawableRes)!!
}

fun ImageView.loadImage(
    src: String?,
    config: RequestBuilder<Drawable>.() -> Unit = {}
) {
    Glide
        .with(context)
        .load(src)
        .apply { config(this) }
        .into(this)
}

fun ImageView.setColorFilterCompat(@ColorRes color: Int) {
    if (color != -1) {
        setColorFilter(ContextCompat.getColor(context, color))
    }
}

fun TextView.setStrikeThroughText(text: String?) {
    setText(text)
    paintFlags = paintFlags or Paint.STRIKE_THRU_TEXT_FLAG
}

val TextView.string get() = this.text.toString()

private var lastClickTimestamp = 0L
fun View.setThrottledClickListener(delay: Long = 200L, onClick: (View) -> Unit) {
    setOnClickListener {
        throttle(delay) {
            onClick(it)
        }
    }
}

private fun throttle(delay: Long = DEFAULT_THROTTLE_DELAY, action: () -> Unit): Boolean {
    val currentTimestamp = System.currentTimeMillis()
    val delta = currentTimestamp - lastClickTimestamp
    if (delta !in 0L..delay) {
        lastClickTimestamp = currentTimestamp
        action()
        return true
    }
    return false
}

fun RecyclerView.setAdapterAndCleanupOnDetachFromWindow(recyclerViewAdapter: RecyclerView.Adapter<*>) {
    adapter = recyclerViewAdapter
    addOnAttachStateChangeListener(object : View.OnAttachStateChangeListener {
        override fun onViewDetachedFromWindow(v: View?) {
            adapter = null
            removeOnAttachStateChangeListener(this)
        }

        override fun onViewAttachedToWindow(v: View?) {
        }
    })
}
package com.applicatura.contacts.base.extensions

import com.applicatura.contacts.base.utils.Either
import com.hannesdorfmann.adapterdelegates4.AbsDelegationAdapter
import kotlinx.coroutines.CancellationException

inline fun <reified T> attempt(func: () -> T): Either<Throwable, T> = try {
    Either.Right(func.invoke())
} catch (e: Throwable) {
    if (e is CancellationException) {
        throw e
    }
    Either.Left(e)
}

fun <T> AbsDelegationAdapter<T>.setData(data: T) {
    items = data
    notifyDataSetChanged()
}
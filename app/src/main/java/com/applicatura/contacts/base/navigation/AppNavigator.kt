package com.applicatura.contacts.base.navigation

import android.content.Intent
import android.os.Handler
import androidx.annotation.IdRes
import androidx.fragment.app.*
import com.applicatura.contacts.features.main.ui.MainFragment
import ru.terrakok.cicerone.android.support.SupportAppNavigator
import ru.terrakok.cicerone.android.support.SupportAppScreen
import ru.terrakok.cicerone.commands.*

/*
   Этот навигатор используется при add fragment и для использования DialogFragment/BottomSheetDialogFragment
 */
open class AppNavigator(
    activity: FragmentActivity,
    fragmentManager: FragmentManager, @IdRes containerId: Int = -1
) : SupportAppNavigator(activity, fragmentManager, containerId) {

    private val handler = Handler()

    override fun applyCommands(commands: Array<out Command>) {
        try {
            super.applyCommands(commands)
        } catch (exception: IllegalStateException) {
            /**
             * Исправляет ошибку "FragmentManager is already executing transactions"
             * https://github.com/terrakok/Cicerone/issues/104
             */
            handler.post { super.applyCommands(commands) }
        }
    }

    override fun applyCommand(command: Command) {
        when (command) {
            is Add -> add(command)
            is Replace -> replace(command)
            is StartWithResult -> startWithResult(command)
            else -> super.applyCommand(command)
        }
    }

    override fun setupFragmentTransaction(
        command: Command,
        currentFragment: Fragment?,
        nextFragment: Fragment?,
        fragmentTransaction: FragmentTransaction
    ) {
        when (command) {
            is Back,
            is BackTo -> fragmentTransaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_CLOSE)

            is Forward,
            is Replace,
            is Add -> {
                if (nextFragment is MainFragment) {
                    fragmentTransaction.setTransition(FragmentTransaction.TRANSIT_NONE)
                } else {
                    fragmentTransaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
                }
            }
        }
    }

    private fun replace(command: Replace) {
        val screen = command.screen as SupportAppScreen
        val fragment = createFragment(screen)

        if (fragment is DialogFragment) {
            forwardDialogFragment(fragment, screen)
        } else
            activityReplace(command)
    }

    private fun startWithResult(command: StartWithResult) {
        val screen = command.screen
        val activityIntent = screen.getActivityIntent(activity)
        if (activityIntent != null) {
            checkAndStartActivity(screen, activityIntent, command.requestCode)
        }
    }

    private fun checkAndStartActivity(screen: SupportAppScreen, activityIntent: Intent, requestCode: Int) {
        if (activityIntent.resolveActivity(activity.packageManager) != null) {
            val fragment = getCurrentFragment()
            if (fragment != null) {
                fragment.startActivityForResult(activityIntent, requestCode)
            } else {
                activity.startActivityForResult(activityIntent, requestCode)
            }
        } else {
            unexistingActivity(screen, activityIntent)
        }
    }

    private fun add(command: Add) {
        val screen = command.screen as SupportAppScreen
        val fragment = createFragment(screen)

        if (fragment is DialogFragment) {
            forwardDialogFragment(fragment, screen)
        } else
            forwardFragmentBase(fragment, command, screen)
    }

    private fun forwardDialogFragment(
        fragment: DialogFragment,
        screen: SupportAppScreen
    ) {
        if (fragmentManager.findFragmentByTag(screen.screenKey) == null)
            fragment.show(fragmentManager, screen.screenKey)
    }

    private fun forwardFragmentBase(
        fragment: Fragment?,
        command: Add,
        screen: SupportAppScreen
    ) {

        val fragmentTransaction = fragmentManager.beginTransaction()

        setupFragmentTransaction(
            command,
            fragmentManager.findFragmentById(containerId),
            fragment,
            fragmentTransaction
        )

        fragment?.let {
            fragmentTransaction
                .add(containerId, it)
                .addToBackStack(screen.screenKey)
                .commit()
            localStackCopy?.add(screen.screenKey)
        }
    }

    private fun getCurrentFragment(): Fragment? =
        fragmentManager.fragments.firstOrNull { !it.isHidden }
}
package com.applicatura.contacts.base.ui.decorators

import android.graphics.Canvas
import android.graphics.Paint
import android.graphics.Rect
import android.view.View
import androidx.annotation.ColorInt
import androidx.core.graphics.withTranslation
import androidx.core.view.children
import androidx.recyclerview.widget.RecyclerView

class DividerItemDecoration(
    @ColorInt private val strokeColor: Int,
    private val strokeWidth: Int,
    private val marginStart: Float = 0.0F,
    private val marginEnd: Float = marginStart,
    private val horizontalSpacing: Int = 0,
    private val verticalSpacing: Int = horizontalSpacing,
    private val withLastItem: Boolean = false
) : RecyclerView.ItemDecoration() {

    private val dividerPaint = Paint().apply {
        color = strokeColor
        flags = Paint.ANTI_ALIAS_FLAG
        strokeWidth = this@DividerItemDecoration.strokeWidth.toFloat()
    }

    override fun getItemOffsets(
        outRect: Rect,
        view: View,
        parent: RecyclerView,
        state: RecyclerView.State
    ) {
        outRect.set(horizontalSpacing, verticalSpacing, horizontalSpacing, verticalSpacing)
    }

    override fun onDrawOver(canvas: Canvas, parent: RecyclerView, state: RecyclerView.State) {
        parent.children.forEach { child ->
            if (child.isNotLastItem(parent, state.itemCount)) {
                canvas.withTranslation(
                    x = 0.0F,
                    y = verticalSpacing.toFloat()
                ) {
                    val lineY = child.bottom - strokeWidth / 2.0F
                    drawLine(
                        child.left + marginStart, lineY, child.right - marginEnd, lineY, dividerPaint
                    )
                }
            }
        }
    }

    private fun View.isNotLastItem(parent: RecyclerView, itemCount: Int) =
        withLastItem || parent.getChildAdapterPosition(this) < itemCount - 1
}
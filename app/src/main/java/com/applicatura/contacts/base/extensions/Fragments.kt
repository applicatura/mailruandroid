package com.applicatura.contacts.base.extensions

import androidx.fragment.app.Fragment

fun Fragment.hideKeyboard() {
    requireActivity().hideKeyboard()
}
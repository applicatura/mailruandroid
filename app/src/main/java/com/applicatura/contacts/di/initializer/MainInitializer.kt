package com.applicatura.contacts.di.initializer

import com.applicatura.contacts.base.navigation.MainActivityRouter
import com.applicatura.contacts.di.DIQualifiers
import org.koin.core.module.Module
import ru.terrakok.cicerone.Cicerone

const val MAIN_ACTIVITY_QUALIFIER = "MAIN_ACTIVITY"

object MainInitializer : Initializer {
    override fun initialize(appModule: Module) {
        appModule.run {

            val ciceroneMainActivityQualifier =
                DIQualifiers.ciceroneQualifier(MAIN_ACTIVITY_QUALIFIER)
            val navigationMainActivityHolderQualifier =
                DIQualifiers.navigationHolderQualifier(MAIN_ACTIVITY_QUALIFIER)

            single<Cicerone<MainActivityRouter>>(ciceroneMainActivityQualifier) {
                Cicerone.create(MainActivityRouter())
            }

            single(navigationMainActivityHolderQualifier) {
                get<Cicerone<MainActivityRouter>>(ciceroneMainActivityQualifier).navigatorHolder
            }

            single {
                get<Cicerone<MainActivityRouter>>(ciceroneMainActivityQualifier).router
            }
        }
    }
}
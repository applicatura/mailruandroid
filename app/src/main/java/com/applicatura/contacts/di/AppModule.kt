package com.applicatura.contacts.di

import com.applicatura.contacts.di.initializer.MainInitializer
import com.applicatura.contacts.di.initializer.SystemInitializer
import org.koin.dsl.module

val appModule = module {
    listOf(
        SystemInitializer,
        MainInitializer,
    ).forEach {
        it.initialize(this)
    }

}

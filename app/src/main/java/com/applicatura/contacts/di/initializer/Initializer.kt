package com.applicatura.contacts.di.initializer

import org.koin.core.module.Module

interface Initializer {

    fun initialize(appModule: Module)
}
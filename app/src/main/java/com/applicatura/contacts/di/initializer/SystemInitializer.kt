package com.applicatura.contacts.di.initializer

import com.applicatura.contacts.base.utils.ResourcesProvider
import com.applicatura.contacts.base.utils.ResourcesProviderImpl
import org.koin.core.module.Module

object SystemInitializer : Initializer {
    override fun initialize(appModule: Module) {
        appModule.run {
            single<ResourcesProvider> { ResourcesProviderImpl(get()) }
        }
    }
}
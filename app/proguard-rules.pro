#### Libraries
# Firebase
-dontwarn com.google.firebase.messaging.FirebaseMessagingService

# okhttp
-dontwarn org.conscrypt.ConscryptHostnameVerifier

# GSON
-keep class * extends com.google.gson.TypeAdapter
-keep class * implements com.google.gson.TypeAdapterFactory
-keep class * implements com.google.gson.JsonSerializer
-keep class * implements com.google.gson.JsonDeserializer

-keepclassmembers,allowobfuscation class * {
  @com.google.gson.annotations.SerializedName <fields>;
}